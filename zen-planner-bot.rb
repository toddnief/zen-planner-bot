require 'rubygems'
require 'sinatra/base'
require 'selenium-webdriver'
require 'json'
require 'net/http'
#require 'pry'

class ZenPlannerBot < Sinatra::Base

  post '/' do

    #send API request to Acuity to get information for appointment based upon appointment ID
    acuity_user_id = 11518015
    acuity_api_key = "cbbef6ed5d1348ac7bf61ba855b761c9"

    uri = URI.parse("https://acuityscheduling.com/api/v1/appointments/#{params['id']}")
    request = Net::HTTP::Get.new(uri)
    request.basic_auth("#{acuity_user_id}", "#{acuity_api_key}")

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    parsed_appointment = JSON.parse(response.body)

    # configure the driver to run in headless mode
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless')
    driver = Selenium::WebDriver.for :chrome, options: options

    # navigate driver to Zen Planner form and fill it out with information from Acuity appointment
    # old form:
    # driver.navigate.to "https://studio.zenplanner.com/zenplanner/studio/widget.html?apiKey=cfb40cdc-9294-4a10-99a8-dfb54fb0894f&widgetInstanceId=47883e4d-3efc-49f4-88ca-3d2d5265e86a&module=leadcapture"
    driver.navigate.to "https://southlooptools.com/zen-planner-form/zen-planner-form.html"
    element = nil
    wait = Selenium::WebDriver::Wait.new(timeout: 10) # seconds
    wait.until { element = driver.find_element(name: "zp-widget-iframe") }
    driver.switch_to.frame driver.find_element(name: "zp-widget-iframe")

    wait.until { element = driver.find_element(id: "first-name") }

    driver.find_element(id: "first-name").send_keys "#{parsed_appointment["firstName"]}"
    driver.find_element(id: "last-name").send_keys "#{parsed_appointment["lastName"]}"
    driver.find_element(id: "email").send_keys "#{parsed_appointment["email"]}"
    driver.find_element(id: "mobilePhone").send_keys "#{parsed_appointment["phone"]}"

    button = driver.find_element(css: "button").click

    sleep 5

    driver.quit
  end

  get '/' do
	'This is the page for Zen Planner Bot — a POST request will add a new member to Zen Planner.'
  end

end

